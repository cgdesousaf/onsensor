import { Injectable } from '@angular/core';

import { RSU } from './rsu';

@Injectable()
export class RsuService {

  private rsus: RSU[] = [];

  constructor() {
    const nOfRSUs = 5;
    for (let i = 0; i < nOfRSUs; i++) {
      // create the RSUs
      const rsu = new RSU();
      const position = {x: Math.floor(Math.random() * 500) + 1,
                        y: Math.floor(Math.random() * 500) + 1};
      rsu.position = position;
      rsu.range = 100;
      this.rsus.push(rsu);
    }
    console.log(nOfRSUs + ' RSUs randomly deployed');
  }

  getRSUs(): RSU[] {
    return this.rsus;
  }

  setRSUs(rsus: RSU[]) {
    this.rsus = rsus;
  }
}
