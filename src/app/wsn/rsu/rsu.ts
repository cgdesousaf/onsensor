/**
 * RSU class implementing Node interface.
 */

import { Node } from '../node';
import { Antenna } from '../antenna';

export class RSU implements Node {
    id: number;
    messageIDs: number[];
    position: {x: number, y: number};
    range: number;
    // antenna: Antenna;

    constructor() {}
    // handleMessages(): void {}
    // getPosition(): {x: number, y: number} {
    //     return this.position;
    // }
}
