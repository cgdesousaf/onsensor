import { Injectable } from '@angular/core';

import { Scenario } from './scenario';

import { SCENARIO } from '../../mock-data';

@Injectable()
export class ScenarioService {
  private scenario: Scenario;
  constructor() {
    this.scenario = SCENARIO;
  }

  getScenario(): Scenario {
    return this.scenario;
  }
}
