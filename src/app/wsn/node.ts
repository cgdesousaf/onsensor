/**
 * Node interface.
 */

import { Antenna } from './antenna';

export interface Node {
    id: number;
    messageIDs: number[];
    position: {x: number, y: number};
    // antenna: Antenna;

    // handleMessages(): void;
    // getPosition(): {x: number, y: number};
}
