import { Injectable } from '@angular/core';

import { Vehicle } from './vehicle';

@Injectable()
export class VehicleService {

  private vehicles: Vehicle[] = [];

  constructor() {
    const nOfVehicles = 100;
    for (let i = 0; i < nOfVehicles; i++) {
        // create the vehicle
        const vehicle = new Vehicle();
        const position = {x: Math.floor(Math.random() * 500) + 1,
                          y: Math.floor(Math.random() * 500) + 1};
        vehicle.position = position;
        vehicle.range = 10;
        this.vehicles.push(vehicle);
    }
    console.log(nOfVehicles + ' vehicles randomly deployed');
  }

  getVehicles(): Vehicle[] {
    return this.vehicles;
  }
}
