/**
 * Antenna interface takes care of communication.
 */

export interface Antenna {
    range: number;

    send(): void;
    receive(): void;
}
