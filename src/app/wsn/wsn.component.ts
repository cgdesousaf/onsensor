import { Component, OnInit, OnChanges} from '@angular/core';

import { Scenario } from './scenario/scenario';
import { Vehicle } from './vehicle/vehicle';
import { RSU } from './rsu/rsu';
import { GeneticAlgorithm } from '../ga/genetic_algorithm';
import { ScenarioService } from './scenario/scenario.service';
import { RsuService } from './rsu/rsu.service';
import { VehicleService } from './vehicle/vehicle.service';
import { GaService } from '../ga/ga.service';

@Component({
  selector: 'app-wsn',
  templateUrl: './wsn.component.html',
  styleUrls: ['./wsn.component.css'],
  providers: [ScenarioService, RsuService, VehicleService, GaService]
})
export class WsnComponent implements OnInit {
  geneticAlgorithm: GeneticAlgorithm;
  scenario: Scenario;
  RSUs: RSU[];
  vehicles: Vehicle[];
  svg: Element;

  ns = 'http://www.w3.org/2000/svg';

  constructor(private scenarioService: ScenarioService, private vehicleService: VehicleService,
    private rsuService: RsuService, private gaService: GaService) {
    this.scenario = this.scenarioService.getScenario();
    this.RSUs = this.rsuService.getRSUs();
    this.vehicles = this.vehicleService.getVehicles();
  }

  ngOnInit() {
    // Get scenario canvas.
    const div = document.getElementById('scenario');
    this.svg = document.createElementNS(this.ns, 'svg');
    this.svg.setAttributeNS(null, 'width', '500');
    this.svg.setAttributeNS(null, 'height', '500');
    div.appendChild(this.svg);

    this.showVehicles(this.vehicles);
    this.showRSUs(this.RSUs);
  }

  findNeighbours(): any {
    const coveredVehicles: {x: number, y: number}[] = [];
    const isCovered = function(array: {x: number, y: number}[], position: {x: number, y: number}){
        for (const p of array){
            if ((p.x === position.x) && (p.y === position.y)) {
                return true;
            }
        }
        return false;
    };
    this.RSUs.forEach(rsu => {
      // TODO: Find RSUs Neighbours and avoid overlap between neighbours!
      this.vehicles.forEach(vehicle => {
          if (isCovered(coveredVehicles, vehicle.position) === false) {
              const distance = Math.sqrt((rsu.position.x - vehicle.position.x) ** 2 + (rsu.position.y - vehicle.position.y) ** 2);
              if (distance <= rsu.range) {
                  coveredVehicles.push(vehicle.position);
              }
          }
      });
    });

    return coveredVehicles;
  }

  showVehicles(vehicles: Vehicle[]): void {
    vehicles.forEach(vehicle => {
      const circle = document.createElementNS(this.ns, 'circle');
      circle.setAttributeNS(null, 'cx', String(vehicle.position.x));
      circle.setAttributeNS(null, 'cy', String(vehicle.position.y));
      circle.setAttributeNS(null, 'r', '2');
      this.svg.appendChild(circle);
    });
  }

  showRSUs(rsus: RSU[]): void {
    rsus.forEach(rsu => {
      const circle = document.createElementNS(this.ns, 'circle');
      circle.setAttributeNS(null, 'cx', String(rsu.position.x));
      circle.setAttributeNS(null, 'cy', String(rsu.position.y));
      circle.setAttributeNS(null, 'r', '6');
      circle.setAttributeNS(null, 'fill', '#FF0000');
      this.svg.appendChild(circle);

      const radius = document.createElementNS(this.ns, 'circle');
      radius.setAttributeNS(null, 'cx', String(rsu.position.x));
      radius.setAttributeNS(null, 'cy', String(rsu.position.y));
      radius.setAttributeNS(null, 'r', String(rsu.range));
      radius.setAttributeNS(null, 'fill', '#FF0000');
      radius.setAttributeNS(null, 'fill-opacity', '0.4');
      this.svg.appendChild(radius);
    });
  }

  run(): void {
    this.geneticAlgorithm = this.gaService.create();
    this.gaService.init(this.scenario, this.vehicles, this.RSUs);
    this.gaService.run();
    const fittest = this.gaService.getFittest();
    const rsus: RSU[] = [];
    fittest.genome.forEach(gene => {
      const rsu = new RSU();
      const position = {x: gene.x, y: gene.y};
      rsu.position = position;
      rsu.range = this.RSUs[0].range;
      rsus.push(rsu);
    });

    this.RSUs = rsus;

    const div = document.getElementById('scenario');
    div.removeChild(this.svg);
    this.svg = document.createElementNS(this.ns, 'svg');
    this.svg.setAttributeNS(null, 'width', '500');
    this.svg.setAttributeNS(null, 'height', '500');
    div.appendChild(this.svg);

    this.showVehicles(this.vehicles);
    this.showRSUs(this.RSUs);

    const coveredVehicles = this.findNeighbours();

    console.log(coveredVehicles);

    coveredVehicles.forEach(vehicle => {
      const circle = document.createElementNS(this.ns, 'circle');
      circle.setAttributeNS(null, 'cx', String(vehicle.x));
      circle.setAttributeNS(null, 'cy', String(vehicle.y));
      circle.setAttributeNS(null, 'r', '2');
      circle.setAttributeNS(null, 'fill', '#FF0000');
      this.svg.appendChild(circle);
    });
  }
}
