/**
 * Genetic Algorithm base class.
 */

import { Individual } from './individual';
import { Scenario } from '../wsn/scenario/scenario';
import { Vehicle } from '../wsn/vehicle/vehicle';
import { RSU } from '../wsn/rsu/rsu';

export { GeneticAlgorithm };

class GeneticAlgorithm {
    population: Individual[] = [];
    numGenerations: number;
    populationSize: number;
    probCrossover: number;
    probMutation: number;

    private scenario: Scenario;
    private vehicles: Vehicle[];
    private rsus: RSU[];

    constructor(numGenerations: number, populationSize: number,
        probCrossover: number, probMutation: number) {
        this.numGenerations = numGenerations;
        this.populationSize = populationSize;
        this.probCrossover = probCrossover;
        this.probMutation = probMutation;
    }
    initialize(scenario: Scenario, vehicles: Vehicle[], rsus: RSU[]): void {
        this.scenario = scenario;
        this.vehicles = vehicles;
        this.rsus = rsus;

        const first: Individual = new Individual();
        this.rsus.forEach(rsu => {
          first.genome.push({x: rsu.position.x, y: rsu.position.y});
        });

        this.population.push(first);
        const genomeLength = first.genome.length;
        for (let i = 1; i < this.populationSize; i++) {
            const individual = new Individual();
            for (let j = 0; j < genomeLength; j++) {
                const pos_x = Math.floor(Math.random() * scenario.size.x) + 1;
                const pos_y = Math.floor(Math.random() * scenario.size.y) + 1;
                individual.genome.push({x: pos_x, y: pos_y});
            }
            this.population.push(individual);
        }
    }
    getFittest(): Individual {
        return this.population[0];
    }
    calcFitness(population: Individual[]): void {
        const rsuRange = this.rsus[0].range;
        let coveredVehicles: {x: number, y: number}[] = [];

        const isCovered = function(array: {x: number, y: number}[], position: {x: number, y: number}) {
            for (const p of array){
                if ((p.x === position.x) && (p.y === position.y)) {
                    return true;
                }
            }
            return false;
        };

        population.forEach(individual => {
            // Get the number of vehicles covered on this network
            let score = 0;
            individual.genome.forEach(gene => {
                // TODO: Find RSUs Neighbours and avoid overlap between neighbours!
                this.vehicles.forEach(vehicle => {
                    if (isCovered(coveredVehicles, vehicle.position) === false) {
                        const distance = Math.sqrt((gene.x - vehicle.position.x) ** 2 + (gene.y - vehicle.position.y) ** 2);
                        if (distance <= rsuRange) {
                            coveredVehicles.push(vehicle.position);
                            score++;
                        }
                    }
                });
            });
            individual.fitness = score;
            coveredVehicles = [];
        });
    }
    tournamentSelection(population: Individual[]): Individual[] {
        const parents: Individual[] = [];
        const getRandomSequence = function(limit, amount, lower_bound, upper_bound): Number[]{
            const unique_random_numbers: Number[] = [];
            if (amount > limit) {
                limit = amount;
            }
            while (unique_random_numbers.length < limit) {
                const random_number = Math.round(Math.random() * (upper_bound - lower_bound) + lower_bound);
                if (unique_random_numbers.indexOf(random_number) === -1) {
                    // Yay! new random number
                    unique_random_numbers.push( random_number );
                }
            }
            return unique_random_numbers;
        };

        const randomPopulation: Number[] = getRandomSequence(this.populationSize, this.populationSize, 0, this.populationSize);

        for (const i of randomPopulation) {
            if ( (Number(i) % 2) === 0 && (Number(i) < randomPopulation.length )) {
                const first: Individual = population[Number(i)];
                const second: Individual = population[Number(i) + 1];
                if (first.fitness > second.fitness) {
                    parents.push(first);
                } else {
                    parents.push(second);
                }
              }
        }

        return parents;
    }
    doCrossover(population: Individual[]): Individual[] {
        const children: Individual[] = [];
        for (let i = 0; i < population.length - 1; i++) {
            if (Number(Math.random) < this.probCrossover) {
                const point = Math.round(Math.random() * (population[i].genome.length - 1 ) + 1);
                const childOne = new Individual();
                childOne.genome = population[i].genome.slice(0, point);
                childOne.genome = childOne.genome.concat(population[i + 1].genome.slice(point, population[i].genome.length -  1));
                const childTwo = new Individual();
                childTwo.genome = population[i + 1].genome.slice(0, point);
                childTwo.genome = childTwo.genome.concat(population[i].genome.slice(point, population[i].genome.length -  1));
                children.push(childOne);
                children.push(childTwo);
            }
        }
        return children;
    }
    doMutation(population: Individual[]): void {
        for (let i = 0; i < population.length - 1; i++) {
            if (Number(Math.random) < this.probMutation) {
                const point = Math.round(Math.random() * (population[i].genome.length - 1 ) + 1);
                population[i].genome[point].x = Math.round(Math.random() * this.scenario.size.x);
                population[i].genome[point].y = Math.round(Math.random() * this.scenario.size.y);
            }
        }
    }
    run(): void {
        for (let i = 0; i < this.numGenerations; i++) {
            this.calcFitness(this.population);
            const parents = this.tournamentSelection(this.population);
            const children = this.doCrossover(parents);
            this.population = this.population.slice(children.length, this.populationSize);
            this.population = children.concat(this.population);
            this.doMutation(this.population);
        }
        this.population.sort(function(a, b){return b.fitness - a.fitness; });
        console.log(this.population[0]);
        console.log('This result covers ' + this.population[0].fitness + ' out of ' + this.vehicles.length + ' vehicles');
    }
}
